resource "aws_cloudwatch_log_group" "petclinic_logs" {
  name_prefix = "petclinic_logs"

  tags = {
    Environment = "test"
    Application = "petlcinic"
  }
}