#-------------------DB RANDOM PASSWORD--------------
resource "random_password" "petclinic_random_password" {
  length  = 16
  special = false
}

resource "aws_ssm_parameter" "petclinic_db_password"{
  name  = "petclinic_db_password"
  type  = "SecureString"
  value = random_password.petclinic_random_password.result
}

#--------------------RDS----------------------------------
resource "aws_db_instance" "petclinic_db" {
  identifier_prefix = "petclinic-"
  allocated_storage = 5
  engine            = "mysql"
  engine_version    = "5.7"
  port              = "3306"

  instance_class = "db.t2.micro"
  name           = "petclinic_database"
  username       = var.DB_USER
  password       = aws_ssm_parameter.petclinic_db_password.value

  availability_zone      = module.vpc.azs[0]
  vpc_security_group_ids = [aws_security_group.petclinic_db_sg.id]
  multi_az               = false
  db_subnet_group_name   = aws_db_subnet_group.petclinic_db_subnet_group.id
  parameter_group_name   = "default.mysql5.7"
  publicly_accessible    = false
  skip_final_snapshot    = true
}

#-------------------RDS SUBNET GROUP-------------
resource "aws_db_subnet_group" "petclinic_db_subnet_group" {
  name_prefix = "petclinic_db_subnet_group-"
  subnet_ids  = [module.vpc.private_subnets[0], module.vpc.private_subnets[1]]
}