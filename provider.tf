provider "aws" {
  region = var.AWS_REGION
}

terraform {
  backend "s3" {
    bucket  = "petclinic-tf-remote-state"
    key     = "terraform.tfstate"
    region  = "eu-central-1"
  }
  
}


