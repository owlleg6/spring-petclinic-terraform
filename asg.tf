resource "aws_launch_configuration" "petclinic_asg_conf" {
  name_prefix          = "petclinic_launch_configuration-"
  image_id             = data.aws_ami.image.id
  instance_type        = var.INSTANCE_TYPE
  iam_instance_profile = aws_iam_instance_profile.petclinic_ecs_service_role.name
  security_groups      = [aws_security_group.petclinic_sg_app.id]

  user_data            = "#!/bin/bash\necho 'ECS_CLUSTER=petclinic_cluster'>/etc/ecs/ecs.config"
}

resource "aws_autoscaling_group" "petclinic_asg" {
  name_prefix           = "petclinic_asg-"
  vpc_zone_identifier   = module.vpc.public_subnets
  launch_configuration  = aws_launch_configuration.petclinic_asg_conf.name
  min_size              = 0
  desired_capacity      = 2
  max_size              = 2
  force_delete          = true

  tag {
    key = "Name"
    value = "petclinic_container"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_attachment" "petclinic_asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.petclinic_asg.id
  alb_target_group_arn   = aws_lb_target_group.petclinic_lb_tg.arn
}