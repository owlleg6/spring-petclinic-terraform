resource "aws_ecs_cluster" "petclinic_cluster" {
  name = "petclinic_cluster"
}

resource "aws_ecs_task_definition" "petclinic_td" {
  family                   = "petclinic_td"
  task_role_arn            = "arn:aws:iam::275362027774:role/ecsTaskExecutionRole"
  container_definitions    = data.template_file.petclinic_ecs_task_definition_template.rendered
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
}

resource "aws_ecs_service" "service" {
  name            = "petclinic_service"
  cluster         = aws_ecs_cluster.petclinic_cluster.id
  task_definition = aws_ecs_task_definition.petclinic_td.arn
  launch_type     = "EC2"
  desired_count   = 4

  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 25
  
  load_balancer {
    target_group_arn = aws_lb_target_group.petclinic_lb_tg.arn
    container_name   = "petclinic_container"
    container_port   = 8080
  }

  depends_on = [aws_lb_listener.petclinic_lb_listener_https]
}