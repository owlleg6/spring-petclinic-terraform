output "aws_lb" {
  description = "link to lb"
  value       = aws_lb.petclinic_lb.dns_name
}