resource "aws_lb" "petclinic_lb" {
  name_prefix        = "pc-lb-"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.petclinic_sg_alb.id]
  subnets            = module.vpc.public_subnets
}

resource "aws_lb_target_group" "petclinic_lb_tg" {
  name_prefix = "pc-tg-"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = module.vpc.vpc_id
  
  deregistration_delay = 10
  health_check {
    healthy_threshold  = 2
  }
}

resource "aws_lb_listener" "petclinic_lb_listener_https" {
  load_balancer_arn = aws_lb.petclinic_lb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:eu-central-1:275362027774:certificate/930355a7-d426-490c-9b30-e50fd28d7f34"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.petclinic_lb_tg.arn
  }
} 

resource "aws_lb_listener" "petclinic_lb_listener_http" {
  load_balancer_arn = aws_lb.petclinic_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"
    
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
} 