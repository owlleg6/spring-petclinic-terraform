resource "aws_iam_role" "petclinic_ecs_instance_role" {
  name_prefix = "petclinic_ecs_instance_role"
  path        = "/"

  assume_role_policy = file("templates/petclinic_ec2_instance_role.json")
}

resource "aws_iam_instance_profile" "petclinic_ecs_service_role" {
  role = aws_iam_role.petclinic_ecs_instance_role.name
}

resource "aws_iam_role_policy_attachment" "petclinic_ecs_instance_role_attachment" {
  role       = aws_iam_role.petclinic_ecs_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}