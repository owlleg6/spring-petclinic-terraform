data "aws_ami" "image" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn-ami-*amazon-ecs-optimized"]
  }
  
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  owners = ["amazon"]
}

data "template_file" "petclinic_ecs_task_definition_template" {
  template = "${file("templates/petclinic_container_definition.json.tpl")}"
  vars     = {
    petclinic_image = var.IMAGE,
    version_tag = var.TAG,
    db_user     = var.DB_USER,
    db_password = aws_ssm_parameter.petclinic_db_password.value,
    db_host     = aws_db_instance.petclinic_db.address,
    db_name     = aws_db_instance.petclinic_db.name,
    log_group   = aws_cloudwatch_log_group.petclinic_logs.name,
    aws_region  = var.AWS_REGION
  }
}

/*------------CERTIFICATE---------------
data "aws_s3_bucket_object" "private_key" {
  bucket = "petclinic-tf-remote-state"
  key    = "./certificate/private.pem"
}

data "aws_s3_bucket_object" "public_key" {
  bucket = "petclinic-tf-remote-state"
  key    = "./certificate/public.pem"
}
*/