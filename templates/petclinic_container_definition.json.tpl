[{
  "name" : "petclinic_container",
  "image" : "${petclinic_image}:${version_tag}",
  "memory" : 400,
  "essential" : true,
  "required_compabilities" : ["EC2"],
  "environment" : [
    {
      "name": "spring.datasource.username",
      "value": "${db_user}"
    },
    {
      "name": "spring.datasource.password",
      "value": "${db_password}"
    },
    {
      "name": "spring.datasource.initialize",
      "value": "yes"
    },
    {
      "name": "spring.profiles.active",
      "value": "mysql"
    },
    {
      "name": "spring.datasource.url",
      "value": "jdbc:mysql://${db_host}/${db_name}"
    }
  ],

  "portMappings" : [
    {
      "containerPort": 8080,
      "hostPort"     : 0,
      "protocol"     : "tcp"
    }
  ],

  "logConfiguration": {
    "logDriver": "awslogs",
    "options": {
      "awslogs-group": "${log_group}",
      "awslogs-region": "${aws_region}"
    }
  }
}]