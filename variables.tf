variable "AWS_REGION" {
  type    = string
  default = "eu-central-1"
}

variable "MY_IP" {
  type    = string
  default = "95.47.56.247/32"
}

variable "INSTANCE_TYPE" {
  type    = string
  default = "t2.micro"
}

variable "DB_USER" {
  type    = string
  default = "admin"
}

variable "IMAGE" {
  type = string
  default = "owlleg68/petclinic_ecs"
}

variable "TAG" {
  type = string
}