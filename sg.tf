#--------------------APP-----------------------
resource "aws_security_group" "petclinic_sg_app" {
  name_prefix = "petclinic_app_sg-"
  vpc_id = module.vpc.vpc_id

  tags = {
    Name = "APP SG"
  }
}

resource "aws_security_group_rule" "ingress_app" {
  type      = "ingress"
  from_port = 0
  to_port   = 0
  protocol  = -1

  source_security_group_id = aws_security_group.petclinic_sg_alb.id

  security_group_id = aws_security_group.petclinic_sg_app.id
}

resource "aws_security_group_rule" "egress_app" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = -1
  cidr_blocks = ["0.0.0.0/0"]
  
  security_group_id = aws_security_group.petclinic_sg_app.id
}

#-------------------ALB-----------------------
resource "aws_security_group" "petclinic_sg_alb" {
  name_prefix   = "petclinic_alb_sg-"
  vpc_id = module.vpc.vpc_id

  tags = {
    Name = "ALB SG"
  }
}

resource "aws_security_group_rule" "ingress_alb_http" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = [var.MY_IP]

  security_group_id = aws_security_group.petclinic_sg_alb.id
}

resource "aws_security_group_rule" "ingress_alb_https" {
  type        = "ingress"
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = [var.MY_IP]

  security_group_id = aws_security_group.petclinic_sg_alb.id
}

resource "aws_security_group_rule" "egress_alb_http" {
  type        = "egress"
  from_port   = 0
  to_port     = 0 
  protocol    = -1
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.petclinic_sg_alb.id
}

#--------------------------DB--------------------------------
resource "aws_security_group" "petclinic_db_sg" {
  name_prefix = "petclinic_db_sg-"
  vpc_id = module.vpc.vpc_id

  tags = {
    Name = "DB SG"
  }
}

resource "aws_security_group_rule" "ingress_db" {
  source_security_group_id = aws_security_group.petclinic_sg_app.id
  security_group_id        = aws_security_group.petclinic_db_sg.id

  type = "ingress"
  from_port = 3306
  to_port = 3306
  protocol = "tcp"
}

resource "aws_security_group_rule" "egress_db" {
  type        = "egress"
  from_port   = 0
  to_port     = 0 
  protocol    = -1
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.petclinic_db_sg.id
}


